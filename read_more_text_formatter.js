/**
 * @file
 * Javascript functionality for Read More and Read Less text switch.
 */

(function($) {
  Drupal.behaviors.readMoreTextFormatter = {
    attach: function (context, settings) {
      $('.read-formatter a.read-more', context).click(function (event) {  
        event.preventDefault();
        $(this).prev('.more-text').show();	
        $(this).hide();                
      });
      $('.read-formatter .more-text a.read-less', context).click(function (event) {  
        event.preventDefault();       
        $(this).parent('span.more-text').hide();
        $(this).parent().parent().find('a.read-more').show();              
      });
    }
  };
})(jQuery);